<?php
return [
	
	'APPLICATION_STATUS' 	=> ['Open'=>'Open','Offered'=>'Offered','Accepted'=>'Accepted', 'Pending Purchase' =>'Pending Purchase', 'Pending Delivery'=>'Pending Delivery', 'Pending Acknowledgement' =>'Pending Acknowledgement', 'Completed'=>'Completed'],

	'PERISHABLE_STATUS' 	=> ['Yes'=>'Yes', 'No'=>'No'],

	'FRAGILE_STATUS' 		=> ['Yes'=>'Yes', 'No'=>'No'],

	'USER_STATUS' 			=> ['Active'=>'Active', 'Deactive'=>'Deactive'],

	'STATUS'	 			=> ['Active'=>'Active', 'Deactive'=>'Deactive'],

	'APPROVE'    			=> ['Approve' => 'Approve', 'Disapprove' => 'Disapprove'],

];

