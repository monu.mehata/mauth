<?php

//User routes

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');


//Admin routes
Route::group(['middleware' => 'admin_guest', 'prefix'=>'admin'], function() {

	//Password register routes
	Route::get('register', 'AdminAuth\RegisterController@showRegistrationForm');
	Route::post('register', 'AdminAuth\RegisterController@register');

	//Password login routes
	Route::get('login', 'AdminAuth\LoginController@showLoginForm');
	Route::post('login', 'AdminAuth\LoginController@login');

	//Password reset routes
	Route::get('password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm');
	Route::post('password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail');
	Route::get('password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
	Route::post('password/reset', 'AdminAuth\ResetPasswordController@reset');

});
	
Route::group(['middleware' => 'admin_auth', 'prefix'=>'admin'], function(){	
	Route::post('logout', 'AdminAuth\LoginController@logout');
	Route::get('dashboard', function(){
		return view('admin.pages.dashboard');
	});
});

//Admin routes for namespace admin
Route::group(['middleware' => 'admin_auth', 'prefix'=>'admin', 'namespace'=> 'Admin'], function(){	
	// users
	Route::get('user/data', 'UserController@data');
	Route::resource('user','UserController');
});
