<!-- begin::Footer -->
<footer class="m-grid__item     m-footer ">
    <div class="m-container m-container--fluid m-container--full-height m-page__container">
        <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
            <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
                <span class="m-footer__copyright">
                    2018 &copy; Developed by
                    <a href="#" class="m-link">
                        Monu mehta
                    </a>
                </span>
            </div>
        </div>
    </div>
</footer>
<!-- end::Footer -->

<section>
    <!--begin::Modal-->
    <div id="DeleteModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        {{ Form::open(array('url' => '', 'method' => 'delete', 'id' => '', 'files'=> true, 'class'=>'DeleteForm ajax_form')) }}
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Delete
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            &times;
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>
                        Are you sure ?
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary m-btn--air" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger m-btn--air">Delete</button>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
    <!--end::Modal-->

    <!--Code for Blank Multiple task -->
    <div id="alert_message_div" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                       Ohh!
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            &times;
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>
                       Please select at least one record.
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary m-btn--air" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>
    <!--End Code for Blank Multiple task -->

    <!--Code for Confirm Multiple Task-->
    <div id="alert_confirm_div" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        {{ Form::open(array('url' => '', 'method' => 'post', 'id' => '', 'files'=> true, 'class'=>'DeleteMultipleForm ajax_form')) }}
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="confirm_alert_message_header"></h5>
                    <input type="hidden" name="ids" value="" id="multiple_Ids">
                    <input type="hidden" name="task" value="" id="task">
                    {{ csrf_field() }}
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            &times;
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <p id="confirm_alert_message_body"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary m-btn--air" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn confirm_btn m-btn--air">Confirm</button>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>

    <!--End Code for Confirm Multiple Task-->   

</section>