<!DOCTYPE html>
<html lang="en" >
    <!-- begin::Head -->
    <head>
        <meta charset="utf-8" />
        <title>
            Metronic | Dashboard
        </title>
        <meta name="description" content="Latest updates and statistic charts">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!--begin::Web font -->
        <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
        <script>
          WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
        </script>
        <!--end::Web font -->
        <!--begin::Base Styles -->
        <link href="{{ asset('public/theme/assets/custom/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="{{ asset('public/theme/assets/custom/datatables.min.css') }}">
        <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"> -->
        <link href="{{ asset('public/theme/assets/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('public/theme/assets/demo/default/base/style.bundle.css') }}" rel="stylesheet" type="text/css" />

        <!--end::Base Styles -->
        <link rel="shortcut icon" href="{{ asset('public/theme/assets/demo/default/media/img/logo/favicon.ico') }}" />
    </head>
    <!-- end::Head -->
    <!-- end::Body -->
    <body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
        <!-- begin:: Page -->
        
        <div class="m-grid m-grid--hor m-grid--root m-page">
            @include('admin.partials.header')
            <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
                @include('admin.partials.menu')
                @yield('content')
            </div>
            @include('admin.partials.footer')
        </div>

        <!--begin::Base Scripts -->
        <script src="{{ asset('public/theme/assets/vendors/base/vendors.bundle.js') }}" type="text/javascript"></script>
        <script src="{{ asset('public/theme/assets/demo/default/base/scripts.bundle.js') }}" type="text/javascript"></script>
        <!--end::Base Scripts -->
        <!--begin::Page Snippets -->
        <script src="{{ asset('public/theme/assets/app/js/dashboard.js') }}" type="text/javascript"></script>
        <!--end::Page Snippets -->
        <script src="{{ asset('public/theme/assets/custom/datatables.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('public/theme/assets/custom/datatables.bootstrap.js') }}" type="text/javascript"></script>
        <script src="{{ asset('public/js/form.submit.js') }}" type="text/javascript"></script>

        <script type="text/javascript">
            @if(isset($type))
            var oTable;
            var columns;
            $(document).ready(function () {
                oTable = $('#ajax-table').DataTable({
                    "oLanguage": {
                        "sProcessing": "<img src='{{ url('public/theme/assets/images/loading.gif') }}' >"
                    },
                    "processing": true,
                    "serverSide": true,
                    "bStateSave": true, 
                    "order": [],
                    "ajax": "{{ url('admin/'.$type.'/data') }}",
                    "pagingType": "full_numbers",
                    "columns": columns,
                    "aoColumnDefs": [
                        { 'bSortable': false, 'aTargets': [ -1, 0 ] }
                    ]
                });
            });
            @endif
            
        </script>
        @yield('scripts')
    </body>
    <!-- end::Body -->
</html>
