@extends('admin.layouts.main')

@section('content')
		
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
	<!-- BEGIN: Subheader -->
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator">
					Users
				</h3>
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="#" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<li class="m-nav__separator">
						-
					</li>
					<li class="m-nav__item">
						<a href="" class="m-nav__link">
							<span class="m-nav__link-text">
								Users Management
							</span>
						</a>
					</li>
					<li class="m-nav__separator">
						-
					</li>
					<li class="m-nav__item">
						<a href="" class="m-nav__link">
							<span class="m-nav__link-text">
								Users
							</span>
						</a>
					</li>
							
				</ul>
			</div>
			<div>
			</div>
		</div>
	</div>
	<!-- END: Subheader -->
	<div class="m-content">
		<div class="m-portlet m-portlet--mobile">
			<div class="m-portlet m-portlet--brand m-portlet--head-solid-bg">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon">
								<i class="flaticon-users"></i>
							</span>
							<h3 class="m-portlet__head-text">
								Managed Users
							</h3>
						</div>
					</div>
				</div>
				<div class="m-portlet__body">
					<div class="">
						<a href="{{ url('admin/user/create')}}" class="btn btn-info m-btn m-btn--icon m-btn--air"><span><i class="la la-plus"></i><span>Add</span></span></a>

						<a href="javascript:void(0);" onClick="checkForNullChecked('Active',this)" data-taskurl="{{ url('user/multitask') }}" class="btn btn-success m-btn m-btn--icon m-btn--air"><span><i class="la la-check "></i><span>Activate</span></span></a>
						
						<a href="javascript:void(0);"  onClick="checkForNullChecked('Inactive',this)" data-taskurl="{{ url('user/multitask') }}" class="btn btn-primary m-btn m-btn--icon m-btn--air"><span><i class="la la-ban"></i><span>Inactivate</span></span></a>
					
						<a href="javascript:void(0);" onClick="checkForNullChecked('Delete',this)" data-taskurl="{{ url('user/multitask') }}" class="btn btn-danger m-btn m-btn--icon m-btn--air"><span><i class="la la-trash"></i><span>Delete</span></span></a>

						<a href="" class="btn btn-accent m-btn m-btn--icon m-btn--air m-btn--icon-only" data-toggle="m-tooltip" title="View" ><i class="la la-eye"></i></a>
					</div>
					<div class="" style="margin-top:30px;">
						<table id="ajax-table" class="table-striped table-hover">
	                        <thead>
	                            <tr>
	                                <th><span><label class="m-checkbox m-checkbox--single m-checkbox--all m-checkbox--solid m-checkbox--brand"><input type="checkbox" class="group-checkable" data-set="#ajax-table .checkboxes"/><span></span></label></span></th>
	                                <th><span>Name</span></th>
	                                <th><span>Email</span></th>
	                                <th><span>Status</span></th>
	                                <th><span>Joined</span></th>
	                                <th><span>Actions</span></th>
	                            </tr>
	                        </thead>
	                        <tbody></tbody>
	                    </table>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
@endsection
@section('scripts')
    <script type="text/javascript"> 
        //datatable columns
        columns = [            
            { data: 'id' },
            { data: 'name' },
            { data: 'email' },
            { data: 'status' },
            { data: 'created_at' },
            { data: 'actions' }
            ];
        //for ajax call in delete multiple
        var route_action = 'user';
    </script>
@endsection
