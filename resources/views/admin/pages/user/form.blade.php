@extends('admin.layouts.main')

@section('content')
		
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
	<!-- BEGIN: Subheader -->
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator">
					Users
				</h3>
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="#" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<li class="m-nav__separator">
						-
					</li>
					<li class="m-nav__item">
						<a href="{{url('admin/user')}}" class="m-nav__link">
							<span class="m-nav__link-text">
								Users Management
							</span>
						</a>
					</li>
					<li class="m-nav__separator">
						-
					</li>
					<li class="m-nav__item">
						<a href="{{ url('admin/user')}}" class="m-nav__link">
							<span class="m-nav__link-text">
								Users
							</span>
						</a>
					</li>
					<li class="m-nav__separator">
						-
					</li>
					<li class="m-nav__item">
						<a href="" class="m-nav__link">
							<span class="m-nav__link-text">
								@if(isset($user))
									Edit User
								@else
									Add User
								@endif
							</span>
						</a>
					</li>
										
				</ul>
			</div>
			<div>
			</div>
		</div>
	</div>
	<!-- END: Subheader -->
	<div class="m-content">
		<!--begin::Portlet-->
		<div class="m-portlet">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<h3 class="m-portlet__head-text">
							@if(isset($user))
								Edit User
							@else
								Add User
							@endif
						</h3>
					</div>
				</div>
			</div>
			<!--begin::Form-->
            @if (isset($user))
            {!! Form::model($user, array('url' => url('admin/user') . '/' . $user->id, 'method' => 'put', 'class' => 'ajax_form m-form m-form--fit m-form--label-align-right', 'files'=> true)) !!}
            @else
            {!! Form::open(array('url' => url('admin/user'), 'method' => 'post', 'class' => 'ajax_form m-form m-form--fit m-form--label-align-right', 'files'=> true )) !!}
            @endif
				<div class="m-portlet__body">

					<div class="form-group m-form__group row">
						{!! Form::label('name', 'Name', array('class' => 'col-form-label col-lg-3 col-sm-12')) !!}
						<div class="col-lg-7 col-md-7 col-sm-12">
							{!! Form::text('name', null, array('class' => 'form-control m-input', 'placeholder' => 'Name')) !!}
						</div>
					</div>

					<div class="form-group m-form__group row">
						{!! Form::label('email', 'Email', array('class' => 'col-form-label col-lg-3 col-sm-12')) !!}
						<div class="col-lg-7 col-md-7 col-sm-12">
							{!! Form::text('email', null, array('class' => 'form-control m-input', 'placeholder' => 'Email')) !!}
						</div>
					</div>


					<div class="form-group m-form__group row">
	                    {!! Form::label('password', 'Password', array('class' => 'col-form-label col-lg-3 col-sm-12')) !!}
	                    <div class="col-lg-7 col-md-7 col-sm-12">
	                        {!! Form::password('password', array('class' => 'form-control m-input', 'placeholder' => 'Password')) !!}
	                    </div>
	                </div>

	                <div class="form-group m-form__group row">
	                    {!! Form::label('password_confirmation', 'Confirm Password', array('class' => 'col-form-label col-lg-3 col-sm-12')) !!}
	                    <div class="col-lg-7 col-md-7 col-sm-12">
	                        {!! Form::password('password_confirmation', array('class' => 'form-control m-input', 'placeholder' => 'Confirm Password')) !!}
	                    </div>
	                </div>


					<div class="form-group m-form__group row">
	                    {!! Form::label('status', 'Status', array('class' => 'col-form-label col-lg-3 col-sm-12')) !!}
	                    <div class="col-lg-7 col-md-7 col-sm-12 m-radio-inline">
							<label class="m-radio m-radio--bold m-radio--state-success">
								{!! Form::radio('status', 'Active', 'true') !!}
								Active
								<span></span>
							</label>
							<label class="m-radio m-radio--bold m-radio--state-brand">
								{!! Form::radio('status', 'Inactive') !!}
								Inactive
								<span></span>
							</label>
						</div>
	                </div>

				</div>

				<div class="m-portlet__foot m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions">
						<div class="row">
							<div class="col-lg-9 ml-lg-auto">
								<button type="submit" class="btn btn-brand m-btn--air">
									Submit
								</button>
								<a href="{{ url('admin/user')}}" class="btn m-btn--air btn-secondary">
									Cancel
								</a>
							</div>
						</div>
					</div>
				</div>
			{!! Form::close() !!}
			<!--end::Form-->
		</div>
		<!--end::Portlet-->
	</div>
	</div>
@endsection
@section('scripts')

@endsection
