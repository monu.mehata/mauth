@extends('admin.layouts.main')

@section('content')
		
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
	<!-- BEGIN: Subheader -->
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator">
					Users
				</h3>
				<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
					<li class="m-nav__item m-nav__item--home">
						<a href="#" class="m-nav__link m-nav__link--icon">
							<i class="m-nav__link-icon la la-home"></i>
						</a>
					</li>
					<li class="m-nav__separator">
						-
					</li>
					<li class="m-nav__item">
						<a href="{{url('admin/user')}}" class="m-nav__link">
							<span class="m-nav__link-text">
								Users Management
							</span>
						</a>
					</li>
					<li class="m-nav__separator">
						-
					</li>
					<li class="m-nav__item">
						<a href="{{ url('admin/user')}}" class="m-nav__link">
							<span class="m-nav__link-text">
								Users
							</span>
						</a>
					</li>
					<li class="m-nav__separator">
						-
					</li>
					<li class="m-nav__item">
						<a href="" class="m-nav__link">
							<span class="m-nav__link-text">
								View Users
							</span>
						</a>
					</li>
										
				</ul>
			</div>
			<div>
			</div>
		</div>
	</div>
	<!-- END: Subheader -->
	<div class="m-content">
		<div class="m-portlet m-portlet--mobile">
			<div class="m-portlet m-portlet--success m-portlet--head-solid-bg">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<span class="m-portlet__head-icon">
								<i class="flaticon-users"></i>
							</span>
							<h3 class="m-portlet__head-text">
								View User Details
							</h3>
						</div>
					</div>
					<div class="m-portlet__head-tools">
						<ul class="m-portlet__nav">
							<li class="m-portlet__nav-item">
								<a href="{{ url('admin/user')}}" class="m-portlet__nav-link btn btn-light m-btn m-btn--pill m-btn--air">Back</a>
							</li>
						</ul>
					</div>
				</div>
				
				<div class="m-portlet__body">
					<ul class="nav nav-tabs" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" data-toggle="tab" href="#m_tabs_1_1">
								User Details
							</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="m_tabs_1_1" role="tabpanel">
							<table class="table m-table table-striped table-hover ">
								<tbody>
									@if($user)
										<tr>
											<th scope="row">Name</th>
											<td>Jhon</td>
										</tr>
										<tr>
											<th scope="row">Email</th>
											<td>Lisa</td>
										</tr>
									@else if
										<tr>
											No record found
										</tr>
									@endif
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
@endsection
@section('scripts')

@endsection
