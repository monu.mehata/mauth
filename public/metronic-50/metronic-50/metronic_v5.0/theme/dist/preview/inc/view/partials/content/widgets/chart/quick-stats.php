<!--begin:: Widgets/Quick Stats-->
<div class="row m-row--full-height">
	<div class="col-sm-12 col-md-12 col-lg-6">
		<div class="m-portlet m-portlet--half-height m-portlet--border-bottom-brand">
			<div class="m-portlet__body">
				<div class="m-widget26">
					<div class="m-widget26__number">
						570
						<small>All Sales</small>
					</div>
					<div class="m-widget26__chart" style="height:90px; width: 220px;">
						<canvas id="m_chart_quick_stats_1"></canvas>
					</div>
				</div>
			</div>
		</div>
		<div class="m--space-30"></div>
		<div class="m-portlet m-portlet--half-height m-portlet--border-bottom-danger">
			<div class="m-portlet__body">
				<div class="m-widget26">
					<div class="m-widget26__number">
						690
						<small>All Orders</small>
					</div>
					<div class="m-widget26__chart" style="height:90px; width: 220px;">
						<canvas id="m_chart_quick_stats_2"></canvas>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-12 col-md-12 col-lg-6">
		<div class="m-portlet m-portlet--half-height m-portlet--border-bottom-success">
			<div class="m-portlet__body">
				<div class="m-widget26">
					<div class="m-widget26__number">
						230
						<small>All Transactions</small>
					</div>
					<div class="m-widget26__chart" style="height:90px; width: 220px;">
						<canvas id="m_chart_quick_stats_3"></canvas>
					</div>
				</div>
			</div>
		</div>
		<div class="m--space-30"></div>
		<div class="m-portlet m-portlet--half-height m-portlet--border-bottom-accent">
			<div class="m-portlet__body">
				<div class="m-widget26">
					<div class="m-widget26__number">
						470
						<small>All Comissions</small>
					</div>
					<div class="m-widget26__chart" style="height:90px; width: 220px;">
						<canvas id="m_chart_quick_stats_4"></canvas>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--end:: Widgets/Quick Stats-->