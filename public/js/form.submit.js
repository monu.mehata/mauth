$(function() {

    $('.ajax_form').on('submit', function(e) {
        var $submitBTN = $(this).find('input[type="submit"]');
        $submitBTN.attr('disabled', 'disabled');
        var posturl = $(this).attr('action');
        var $this = $(this).closest('form');
        var formID = $(this).attr('id');
        var formClass = $(this).attr('class');
        var loadingHTML = '<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i>'
        $submitBTN.append(loadingHTML);
        if (!formID)
            formID = formClass;
        window.ajaxRequested = true;
        $($this).find('.form-group').removeClass('has-error');
        $($this).find('.help-block').hide();
        thisform = $this;

        $.each($this.find('input'), function(key, value) {
            if (!$(this).val())
                $(this).removeClass('edited');
        });

        $.ajaxSetup({
            header: $('meta[name="csrf-token"]').attr('content')
        })
        e.preventDefault(e);
        $.ajax({
            type: "POST",
            url: posturl,
            data: new FormData(this),
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function(response) {
                checkTosterResponse(response);
                response.formID = formID;
                $submitBTN.removeAttr('disabled');
                $submitBTN.find('.fa-spin').remove();

                window.ajaxRequested = false;
                $($this).find('.ajax_alert').removeClass('alert-danger').removeClass('alert-success');
                $($this).find('.ajax_alert').fadeOut('fast');

                if (response.success) {
                    $($this).find('.ajax_alert').addClass('alert-success');
                    window.madeChangeInForm = false;
                } else {
                    $($this).find('.ajax_alert').addClass('alert-danger');
                }
                if (response.message) {
                    if (response.notify) {
                        $($this).find('.ajax_alert').fadeIn('slow').children('.ajax_message').html(response.message);
                    } else {
                        $($this).find('.ajax_alert').fadeIn('slow').children('.ajax_message').html(response.message);
                    }
                }
                if (response.redirectURL)
                    window.location.href = response.redirectURL;
                if (response.scrollToThisForm)
                    scrollToElement('#' + formID, 1000);
                if (response.selfReload)
                    window.location.reload();
                if (response.resetForm)
                    $($this).resetForm();
                if (response.callBackFunction)
                    callBackMe(response.callBackFunction, response);
                $(thisform).find('.form-group').removeClass('has-error');
            },
            error: function(response) {
                checkErrorTosterResponse(response.responseJSON);
                $submitBTN.removeAttr('disabled');
                $submitBTN.find('.fa-spin').remove();
            }

        })
    });

    function callBackMe(functionName, data) {
        window[functionName](data);
    }

    function scrollToElement(element, speed) {
        $('html, body').animate({ scrollTop: $(element).position().top + 100 }, speed);
    }

    function checkTosterResponse(data) {
        if (data) {
            if (data.message) {
                if (data.success) {
                    toastr.success(data.message);
                } else {
                    toastr.error(data.message);
                }
            }
        }
    }

    function checkErrorTosterResponse(data) {
        if (data) {
            var string = '';
            $.each(data.errors, function(index, value) {
                string = '<p>' + string + value[0] + '</p>';
            });
            toastr.error(string);
        }
    }

});

 //Delete Record
function deleteRecord($this) {
    $('#DeleteModal').modal('show');
    $('.DeleteForm').attr('action', $($this).data('url'));
}

function callBackCommonDelete(data)
{
    $('#DeleteModal').modal('hide');
    $('#alert_confirm_div').modal('hide');
    location.reload();
}
function regenerate($this) {
    $('#regenerateModal').modal('show');
    $('.regenerateForm').attr('action', $($this).data('url'));
}


//Code for Check Checkbox is not Blank
function checkForNullChecked(task,$this)
{
    var selected = new Array();
    $("#ajax-table input.recordcheckbox:checked").each(function() {
        selected.push($(this).val());
    });
    if( selected.length==0 )
    {
        $('#alert_message_div').modal({
            backdrop: 'static',
            keyboard: false
        });

    } else {
        var taskurl = $($this).attr('data-taskurl');
        showConfirmDialogTableMultiple(task,taskurl);
    }
}

//Show Confirm Dialog Popup 
function showConfirmDialogTableMultiple(task,taskurl)
{
    $('#alert_confirm_div').modal({
        backdrop: 'static',
        keyboard: false
    });
    $('#alert_confirm_div').find('#confirm_alert_message_header').text('Please confirm');
    $('#alert_confirm_div').find('#confirm_alert_message_body').text('Are you sure? you want to '+task+ ' this record.');

    if(task=='Active') {
        $('#alert_confirm_div').find('.confirm_btn').removeClass('btn-primary');
        $('#alert_confirm_div').find('.confirm_btn').removeClass('btn-danger');
        $('#alert_confirm_div').find('.confirm_btn').addClass('btn-success');
    
    } else if(task=='Inactive') {
        $('#alert_confirm_div').find('.confirm_btn').removeClass('btn-danger');
        $('#alert_confirm_div').find('.confirm_btn').removeClass('btn-success');
        $('#alert_confirm_div').find('.confirm_btn').addClass('btn-primary');

    } else if(task=='Delete') {
        $('#alert_confirm_div').find('.confirm_btn').removeClass('btn-success');
        $('#alert_confirm_div').find('.confirm_btn').removeClass('btn-primary');
        $('#alert_confirm_div').find('.confirm_btn').addClass('btn-danger');
    }

    $('#alert_confirm_div').find('.DeleteMultipleForm').attr('action',taskurl);
    var selected = new Array();
    $("#ajax-table input.recordcheckbox:checked").each(function() {
        selected.push($(this).val());
    });
    $('#alert_confirm_div').find('#multiple_Ids').val(selected);
    $('#alert_confirm_div').find('#task').val(task);
}   

//select all checkbox
$(document).on('change','#ajax-table .group-checkable',function () {
    $("input:checkbox").prop('checked', $(this).prop("checked"));
});