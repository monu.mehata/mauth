//== Class Definition
var SnippetLogin = function() {

    var login = $('#m_login');

    var displaySignUpForm = function() {
        login.removeClass('m-login--forget-password');
        login.removeClass('m-login--signin');

        login.addClass('m-login--signup');
        login.find('.m-login__signup').animateClass('flipInX animated');
    }

    var displaySignInForm = function() {
        login.removeClass('m-login--forget-password');
        login.removeClass('m-login--signup');

        login.addClass('m-login--signin');
        login.find('.m-login__signin').animateClass('flipInX animated');
    }

    var displayForgetPasswordForm = function() {
        login.removeClass('m-login--signin');
        login.removeClass('m-login--signup');

        login.addClass('m-login--forget-password');
        login.find('.m-login__forget-password').animateClass('flipInX animated');
    }

    var handleFormSwitch = function() {
        $('#m_login_forget_password').click(function(e) {
            e.preventDefault();
            displayForgetPasswordForm();
        });

        $('#m_login_forget_password_cancel').click(function(e) {
            e.preventDefault();
            displaySignInForm();
        });

        $('#m_login_signup').click(function(e) {
            e.preventDefault();
            displaySignUpForm();
        });

        $('#m_login_signup_cancel').click(function(e) {
            e.preventDefault();
            displaySignInForm();
        });
    }

    var handleSignInFormSubmit = function() {
        $('#m_login_signin_submit').click(function(e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

            form.ajaxSubmit({
                url:base_url+'/admin/login',
                type: "POST",
                success: function(response) {
                    btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    checkTosterResponse(response);
                    if (response.redirectURL)
                        window.location.href = response.redirectURL;
                },
                error: function(response) {
                    console.log(response.responseJSON);
                    checkErrorTosterResponse(response.responseJSON);
                    btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                }
            });

        });
    }


    var handleForgetPasswordFormSubmit = function() {
        $('#m_login_forget_password_submit').click(function(e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');

            btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);

            form.ajaxSubmit({
                type: "POST",
                url: base_url+'admin/password/email',
                success: function(response, $form) {
                    checkTosterResponse(response);
                    btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    form.clearForm();
                    form.resetForm();
                     displaySignInForm();
                    var signInForm = login.find('.m-login__signin form');
                    signInForm.clearForm();
                    signInForm.resetForm();
                },
                error: function(response) {
                    checkErrorTosterResponse(response.responseJSON);
                    btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                }
            });
        });
    }

    
    return {
        // public functions
        init: function() {
            handleFormSwitch();
            handleSignInFormSubmit();
            handleForgetPasswordFormSubmit();
        }
    };

    function checkErrorTosterResponse(data) {
        if (data) {
            var string = '';
            $.each(data.errors, function(index, value) {
                string = '<p>' + string + value[0] + '</p>';

            });
            toastr.error(string);
        }
    }

    function callBackMe(functionName, data) {
        window[functionName](data);
    }

    function scrollToElement(element, speed) {
        $('html, body').animate({ scrollTop: $(element).position().top + 100 }, speed);
    }

    function checkTosterResponse(data) {
        if (data) {
            if (data.message) {
                if (data.success) {
                    toastr.success(data.message);
                } else {
                    toastr.error(data.message);
                }
            }
        }
    }

}();

//== Class Initialization
jQuery(document).ready(function() {
    SnippetLogin.init();
});