<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'name'      => 'required|min:3|max:255',
                    'email'     => 'required|email|unique:users,email',
                    'password'  => array(
                                      'required',
                                      'min:6',
                                      'max:20',
                                      'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*(_|[^\w])).+$/',
                                      'confirmed',
                                    ),
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'name'      => 'required|min:3|max:255',
                    'email'     => 'required|email|unique:users,email,'.$this->user->id.',id',
                    'password'  => array(
                                      'nullable',
                                      'min:6',
                                      'max:20',
                                      'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*(_|[^\w])).+$/',
                                      'confirmed',
                                    ),
                    'password_confirmation' => 'nullable|min:6|max:20'
                ];
            }
            default:break;
        }
    }

    public function messages()
    {
        return [
            'password.regex' => 'Password must contain at least 1 number, 1 uppercase character, 1 lowercase character, and 1 special character.',
            'password.confirmed' => 'Password and confirm password do not match.',
        ];
    }
}
