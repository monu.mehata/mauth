<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Config;
use App\Http\Requests\Admin\UserRequest;
use App\User;

class UserController extends Controller
{
    public function __construct()
    {
        view()->share('type', 'user');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.pages.user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $status = Config::get('constants.USER_STATUS');
        return view('admin.pages.user.form',compact('status'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $user = User::create([
            'name'       => $request->name,
            'email'      => $request->email,
            'password'   => bcrypt($request->password),
            'status'     => $request->status
        ]);

        $output                 = array();
        $output['success']      = true;
        $output['message']      = "User added sucessfully!";
        $output['redirectURL']  = url('admin/user');

        return response()->json($output);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('admin.pages.user.view', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('admin.pages.user.form', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(User $user, UserRequest $request)
    {
        $user->name     = $request->name;
        $user->email    = $request->email;
        $user->status   = $request->status;
        
        if($request->password)
            $user->password = bcrypt($request->password);

        $user->update();
        
        $output                 = array();
        $output['success']      = true;
        $output['message']      = 'User updated successfully.';
        $output['redirectURL']  = url('admin/user');

        return response()->json($output);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user_deleted = $user->delete();

        $output                 = array();
        $output['success']      = true;
        $output['message']      = trans('content.user').' '.trans('content.deleted-successfully');
        $output['redirectURL']  = url('admin/user');

        return response()->json($output);
    }

     /**
     * Remove the selected resource from storage.
     *
     * @param $faq
     * @return view
     */
    public function multitaskOperation(Request $request)
    {
        $task = Input::get('task');
        $ids = Input::get('ids');
        $dataIds = explode(',',$ids);

        $output = array();
        foreach ($dataIds as $key => $value) 
        {
            if($task == 'Delete') {
                $user = User::find($value); 
                $user->delete();
                $message = trans('content.user').' '.trans('content.deleted-successfully');
            }
            else if($task=='Active' || $task=='Inactive') {
                $user = User::find($value); 
                if($task=='Active'){
                    $user->status = '1';
                    $message  = trans('content.user').' '.trans('content.activated-successfully');
                } else {
                    $user->status = '0';
                    $message  = trans('content.user').' '.trans('content.inactivated-successfully');
                }
                $user->update();
            }
        }

        $output['success']      = true;
        $output['message']      = $message; 
        $output['redirectURL']  = url('user');

        return response()->json($output);
    }

    /**
     * Show a list of all the languages posts formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function data()
    {
        $users = User::latest('created_at')->get();
        
        return Datatables::of($users)

        ->addColumn('actions', '

        <a href="{{{ url(\'admin/user/\' . $id ) }}}"  class="btn btn-accent m-btn m-btn--icon m-btn--icon-only m-btn--air" data-toggle="m-tooltip" title="View" ><i class="la la-eye"></i></a>

        <a href="{{{ url(\'admin/user/\' . $id . \'/edit\' ) }}}" class="btn btn-brand m-btn m-btn--icon m-btn--icon-only m-btn--air" data-toggle="m-tooltip" title="Edit" ><i class="la la-edit"></i></a>

        <a href="javascript:void(0);" data-url="{{{ url(\'admin/user/\' . $id ) }}}" onClick="deleteRecord(this);" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only m-btn--air" data-toggle="m-tooltip" title="Delete" ><i class="la la-trash"></i></a>
            ')

        ->editColumn('id', '<label class="m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--brand"><input value="{{{$id}}}" type="checkbox" class="checkboxes recordcheckbox"><span></span></label>')

        ->editColumn('status', '<span class="m-badge m-badge--success m-badge--wide m-btn--air ">{{{$status}}}</span>')

        ->rawColumns(['id','actions', 'status'])

        ->make(true);
    }
}

//<span class="m-badge  m-badge--success m-badge--wide">Success</span>
// <a href="{{{ url(\'admin/user/\' . $id . \'/view\' ) }}}" class="btn btn-success btn-sm iframe" ><i class="fa fa-eye"></i> View</a>
//             <a href="{{{ url(\'admin/user/\' . $id . \'/edit\' ) }}}" class="btn btn-warning btn-sm iframe" ><i class="fa fa-pencil"></i> Edit</a>
//             <a href="javascript:void(0);" class="btn btn-sm btn-danger iframe js_data_delete" data-path="{{{ url(\'admin/user/\' . $id . \'/delete\' ) }}}"><i class="fa fa-remove"></i> Delete</a>
