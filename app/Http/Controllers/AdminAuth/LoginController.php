<?php
namespace App\Http\Controllers\AdminAuth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class LoginController extends Controller
{

    //Trait
    use AuthenticatesUsers;

    //Custom guard for admin
    protected function guard()
    {
      	return Auth::guard('web_admin');
    }

    //Custom logout for admin
    public function logout(Request $request)
    {
        $this->guard('web_admin')->logout();

        return redirect('/admin/login');
    }

    //Shows admin login form
   	public function showLoginForm()
   	{
    	return view('admin.auth.login');
   	} 

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        $output['success']      = true;
        $output['message']      = 'You are successfully logged in';
        $output['redirectURL']  = url('/admin/dashboard');

        return response()->json($output);

    }
    /**
      * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        $errors = [$this->username() => trans('auth.failed')];
        $output = array();
        $output['success']      = false;
        $output['message']      = $errors['email'];
        return response()->json($output);
    }
    
   
}
